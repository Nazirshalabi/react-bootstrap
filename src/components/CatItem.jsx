import {ListGroup} from 'react-bootstrap'
 
function CatItem({ cat, onClick }) {
  return (
    <ListGroup.Item action variant="light" className="my-list-item" onClick={() => onClick(cat)}>
      <span className="list_view">
        <img src={cat.url} alt="cat" />
        <span>Name: {cat.name}</span>
      </span>
      <p>Birthdate: {cat.birthdate}</p>
    </ListGroup.Item>
  );
}

export default CatItem;
