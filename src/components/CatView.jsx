import React from "react";
import { useContext } from "react";
import { CatContext } from "../CatContext";
import CatForm from "./CatForm";
import Modal from 'react-bootstrap/Modal'
import Catform from "./Form"

function CatView() {
  const [{ activeCat, isEditing }, dispatch] = useContext(CatContext);

  function handleCateDeletion() {
    if (window.confirm("Are you sure you want to delete this cat?")) {
      dispatch({ type: "delete_active" });
    }
  }

  if (!activeCat) {
    return null;
  }
  const styles ={
    width: "600px",
    height: "700px",
    position: "absolute",
    left: "50%",
    top: "50%",
    margin: "-150px",
    opacity:"1"
  }

  return (
    <div className="view">
      <div className="view-content">
        <img src={activeCat.url} alt="cat" />
        <span>Name: {activeCat.name}</span>
        <span> Birthdate: {activeCat.birthdate}</span>
        <span>Owner: {activeCat.owner}</span>
        <span>Number of views: {activeCat.view}</span>
      </div>
      <div className="view-buttons">
        <button
          type="button"
          onClick={() => {
            dispatch({ type: "toggle_editing" });
          }}
        >
          Edit
        </button>
        <span>&nbsp;&nbsp;|&nbsp;&nbsp;</span>
        <button onClick={handleCateDeletion}>Delete</button>
      </div>
      <Modal show={isEditing} style={styles} dialogClassName="my-modal">
      < Modal.Header >
          <Modal.Title>Editing {activeCat.name}</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Catform />
        </Modal.Body>
        
      </Modal>
    </div>
  );
}

export default CatView;
