import React from "react";
import { useContext } from "react";
import { CatContext } from "../CatContext";
import CatItem from "./CatItem";
import {ListGroup} from 'react-bootstrap'

function CatList() {
  const [{ cats }, dispatch] = useContext(CatContext);
  const handleCatSelection = (cat) => {
    dispatch({ type: "change_view", cat });
  };
  return (
    <ListGroup className="list">
      {cats.map((cat) => (
        <CatItem key={cat.id} cat={cat} onClick={handleCatSelection} />
      ))}
    </ListGroup>
  );
}

export default CatList;
