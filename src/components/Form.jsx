import React from "react";
import { useForm } from "react-hook-form";
import { CatContext } from "../CatContext";
import {Row, Col, Button, Form}from 'react-bootstrap'


function Catform(){


  const [{ activeCat}, dispatch] = React.useContext(CatContext);
  const { register, handleSubmit, watch, formState: { errors } } = useForm();
  const onSubmit = data => {
    //e.preventDefault();
    dispatch({ type: "update_cat", cat: {...activeCat,...data}});
    dispatch({type: "toggle_editing"})
  }


  console.log(watch())
  return (
    /* "handleSubmit" will validate your inputs before invoking "onSubmit" */
    
        <Form onSubmit={handleSubmit(onSubmit)}>
            <Form.Group >
                <Form.Label>Thumbnail URL :{" "}</Form.Label>
                <Form.Control placeholder="Enter url" type="url"
                    id="url"
                    {...register("url", { required: true })}/>
                
            </Form.Group>
            <Form.Group>
                <Form.Label>Name: {""}</Form.Label>
                <Form.Control placeholder="Enter New Name" type="String" id="name"
                    {...register("name", { required: true })} />
            </Form.Group>
            <Form.Group>
                <Form.Label> Birthdate: </Form.Label>
                <Form.Control type="date"
                    id="birthdate"
                    {...register("birthdate", { required: true })}/>
            </Form.Group>
            <Form.Group>
                <Form.Label>Owner: </Form.Label>
                <Form.Control id="owner"
                    {...register("owner", { required: true })} as="select" size="lg">
                    <option>Natheer</option>
                    <option>Antonio</option>
                    <option>Justin</option>
                    <option>Mike</option>
                    <option>Nazir</option>
                </Form.Control>
            </Form.Group>

            <div className="modal-footer">
                <Button variant="primary"type="submit">Save</Button>
                <span>&nbsp;&nbsp;|&nbsp;&nbsp;</span>
                <Button variant="danger"
                onClick={() => {
                    dispatch({ type: "toggle_editing" });
                }}
                >
                Cancel
                </Button>
            </div>
        </Form>
  );
}

export default Catform;