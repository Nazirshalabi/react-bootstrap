
import React from "react";
import { useState } from "react";
import { CatContext } from "../CatContext";

function CatForm() {
  const [state, dispatch] = React.useContext(CatContext);
  const [localCat, setLocalCat] = useState({ ...state.activeCat });
  if (localCat.id !== state.activeCat.id) {
    setLocalCat(state.activeCat);
  }

  const { url, name, birthdate, owner } = localCat;

  function handleInputChange(e) {
    setLocalCat({ ...localCat, [e.target.name]: e.target.value });
  }

  const handleSubmit = (e) => {
    e.preventDefault();
    dispatch({ type: "update_cat", cat: localCat });
    dispatch({ type: "toggle_editing" });
  };

  return (
    <div
      className="modals fade show"
      tabIndex="-1"
      role="dialog"
      aria-hidden="true"
      style={{ display: "block" }}
    >
      <div className="modal-dialog" role="document">
        <div className="modal-content">
          <div className="modal-header">
            <h5 className="modal-title">Edit Cat</h5>
            <button className="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          
        </div>
      </div>
    </div>
  );
}

export default CatForm;
