import React from 'react';
import './App.css';
import {CatProvider} from "./CatContext";
import CatList from "./components/CatList";
import CatView from "./components/CatView";

function App() {
  return (
    <div className="App">
        <div className="container">
            <CatProvider>
                <CatList />
                <CatView />
            </CatProvider>
        </div>
    </div>
  );
}

export default App;